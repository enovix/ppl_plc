﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="Permissives" Id="{a67f9ea3-9eb0-452a-a056-c2d8b692d68d}" SpecialFunc="None">
    <Declaration><![CDATA[PROGRAM Permissives
VAR
	perm_default : PERMISSIVE := (is_active:=FALSE);
    default_permissive_list : ARRAY [0..9] OF PERMISSIVE := [10(perm_default)];

	//permissive definitions
	example_permissive : PERMISSIVE;

	heat_stake_cooled_off_1: PERMISSIVE;
	heat_stake_cooled_off_2: PERMISSIVE;
	heat_stake_cooled_off_3: PERMISSIVE;
	
	cell_rotator_axis_action_complete : PERMISSIVE;
	cell_rotator_axis_idle : PERMISSIVE;
	
	stage_axis_action_complete : PERMISSIVE;
	stage_axis_idle: PERMISSIVE;
	
	PPL_dispense_axis_action_complete : PERMISSIVE;
	PPL_dispense_axis_idle: PERMISSIVE;
	
	gripper_assm_y_extended: PERMISSIVE;
	gripper_assm_y_retracted: PERMISSIVE;
	
	gripper_assm_z_extended: PERMISSIVE;
	gripper_assm_z_retracted: PERMISSIVE;
	
	cell_support_y_extended: PERMISSIVE;
	cell_support_y_retracted: PERMISSIVE;
	
	cell_support_z_extended: PERMISSIVE;
	cell_support_z_retracted: PERMISSIVE;
	
	cell_rotator_extended: PERMISSIVE;
	cell_rotator_retracted: PERMISSIVE;
	
	strip_holder_left_extended: PERMISSIVE;
	strip_holder_left_retracted: PERMISSIVE;
	
	strip_holder_right_extended: PERMISSIVE;
	strip_holder_right_retracted: PERMISSIVE;
	
	heat_stake_left_extended: PERMISSIVE;
	heat_stake_left_retracted: PERMISSIVE;
	
	heat_stake_right_extended: PERMISSIVE;
	heat_stake_right_retracted: PERMISSIVE;
	
	cell_gripper_extended: PERMISSIVE;
	cell_gripper_retracted: PERMISSIVE;
	
	alignment_gripper_extended: PERMISSIVE;
	alignment_gripper_retracted: PERMISSIVE;
	
	recipe_selection_completed: PERMISSIVE;
	PPL_under_sensor: PERMISSIVE;
	PPL_not_under_sensor: PERMISSIVE;
	decouple_ppl_and_rotator_complete: PERMISSIVE;
	couple_ppl_and_rotator_complete: PERMISSIVE;
	
	true_after_step_action : PERMISSIVE;

END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[
example_permissive.description := 'this is an example_permissive';

true_after_step_action.description := 'Inverse of the Step Action RTrig bit';
true_after_step_action.status := NOT MAIN.state_machine.out_perform_step_action.Q;

//==================================== drives ===================================
//-------- rotator axis
cell_rotator_axis_action_complete.description := 'Commanding Cell Rotator Drive';
cell_rotator_axis_action_complete.status:= MAIN.rotator_axis.out_action_complete;
cell_rotator_axis_idle.description:= 'Cell Rotator Axis is Idle';
cell_rotator_axis_idle.status:= MAIN.rotator_axis.out_idle;

//-------- stage axis
stage_axis_action_complete.description := 'Commanding Stage Drive';
stage_axis_action_complete.status:= MAIN.stage_axis.out_action_complete;
stage_axis_idle.description:= 'Stage Axis is Idle';
stage_axis_idle.status:= MAIN.stage_axis.out_idle;

//-------- PPL dispense axis
PPL_dispense_axis_action_complete.description := 'Commanding PPL Dispense Drive';
PPL_dispense_axis_action_complete.status:= MAIN.PPL_dispense_axis.out_action_complete;

PPL_dispense_axis_idle.description:= 'PPL Dispense Axis is Idle';
PPL_dispense_axis_idle.status:= MAIN.PPL_dispense_axis.out_idle;

//---------Couple ppl dispense axis and rotator axis
decouple_ppl_and_rotator_complete.description:= 'Decoupled PPL and Rotator Axis';
IF main.PPL_dispense_axis_ref.NcToPlc.CoupleState = 0 THEN
	decouple_ppl_and_rotator_complete.status:= TRUE;
ELSE
	decouple_ppl_and_rotator_complete.status:= FALSE;	
END_IF

couple_ppl_and_rotator_complete.description:= 'Coupled PPL and Rotator Axis';
IF main.PPL_dispense_axis_ref.NcToPlc.CoupleState = 3 THEN
	couple_ppl_and_rotator_complete.status:= TRUE;
ELSE
	couple_ppl_and_rotator_complete.status:= FALSE;	
END_IF

//==================================== Actuators ===================================
// ------ Gripper Assembly Y
gripper_assm_y_extended.description := 'Extending Gripper Assembly Y';
gripper_assm_y_extended.status := MAIN.cylinders.gripper_assm_y_extended;

gripper_assm_y_retracted.description := 'Retracting Gripper Assembly Y';
gripper_assm_y_retracted.status := MAIN.cylinders.gripper_assm_y_retracted;

// ------ Gripper Assembly Z
gripper_assm_z_extended.description := 'Extending Gripper Assembly Z';
gripper_assm_z_extended.status := MAIN.cylinders.gripper_assm_z_extened;

gripper_assm_z_retracted.description := 'Retracting Gripper Assembly Z';
gripper_assm_z_retracted.status := MAIN.cylinders.gripper_assm_z_retracted;

// ------ Cell Support Y
cell_support_y_extended.description := 'Extending Cell Support Y';
cell_support_y_extended.status := MAIN.cylinders.cell_support_y_extended;

cell_support_y_retracted.description := 'Retracting Cell Support Y';
cell_support_y_retracted.status := MAIN.cylinders.cell_support_y_retracted;

// ------ Cell Support Z
cell_support_z_extended.description := 'Extending Cell Support Z';
cell_support_z_extended.status := MAIN.cylinders.cell_support_z_extended;

cell_support_z_retracted.description := 'Retracting Cell Support Z';
cell_support_z_retracted.status := MAIN.cylinders.cell_support_z_retracted;

// ------ cell rotator 
cell_rotator_extended.description := 'Extending Cell Rotator';
cell_rotator_extended.status := MAIN.cylinders.rotator_extended;

cell_rotator_retracted.description := 'Retracting Cell Rotator';
cell_rotator_retracted.status := MAIN.cylinders.rotator_retracted;

// ------ strip holder 1 
strip_holder_left_extended.description := 'Extending strip holder 1';
strip_holder_left_extended.status := MAIN.cylinders.strip_holder_left_extended;

strip_holder_left_retracted.description := 'Retracting strip holder 1';
strip_holder_left_retracted.status := MAIN.cylinders.strip_holder_left_retracted;

// ------ strip holder 2
strip_holder_right_extended.description := 'Extending strip holder 2';
strip_holder_right_extended.status := MAIN.cylinders.strip_holder_right_extended;

strip_holder_right_retracted.description := 'Retracting strip holder 2';
strip_holder_right_retracted.status := MAIN.cylinders.strip_holder_right_retracted;

// ------ heat stake 1
heat_stake_left_extended.description := 'Extending heat stake 1';
heat_stake_left_extended.status := MAIN.cylinders.heat_stake_left_extended;

heat_stake_left_retracted.description := 'Retracting heat stake 1';
heat_stake_left_retracted.status := MAIN.cylinders.heat_stake_left_retracted;

// ------heat stake 2
heat_stake_right_extended.description := 'Extending heat stake 2';
heat_stake_right_extended.status := MAIN.cylinders.heat_stake_right_extended;

heat_stake_right_retracted.description := 'Retracting heat stake 2';
heat_stake_right_retracted.status := MAIN.cylinders.heat_stake_right_retracted;

// ------ cell gripper
cell_gripper_extended.description := 'Extending cell gripper';
cell_gripper_extended.status := MAIN.cylinders.gripper_extended;

cell_gripper_retracted.description := 'Retracting cell gripper';
cell_gripper_retracted.status := MAIN.cylinders.gripper_retracted;

// ------ cell alignment 
alignment_gripper_extended.description := 'Extending cell alignment';
alignment_gripper_extended.status := MAIN.cylinders.aligner_extended;

alignment_gripper_retracted.description := 'Retracting cell alignment';
alignment_gripper_retracted.status := MAIN.cylinders.aligner_retracted;

//==================================== HMI ===================================
recipe_selection_completed.description:= 'User Select Recipe';
recipe_selection_completed.status:= MAIN.recipe_manager.out_set;

//==================================== Sensors =================================== 
PPL_under_sensor.description:= 'PPL Under Sensor';
PPL_under_sensor.status:= Main.ppl_dispense_home_sensor;

PPL_not_under_sensor.description:= 'PPL Not Under Sensor';
PPL_not_under_sensor.status:= NOT Main.ppl_dispense_home_sensor;

//============================= timers ========================================
heat_stake_cooled_off_1.description:= 'PPL has been cooled off';
heat_stake_cooled_off_1.status:= main.cool_off_1_complete;

heat_stake_cooled_off_2.description:= 'PPL has been cooled off';
heat_stake_cooled_off_2.status:= main.cool_off_2_complete;

heat_stake_cooled_off_3.description:= 'PPL has been cooled off';
heat_stake_cooled_off_3.status:= main.cool_off_3_complete;]]></ST>
    </Implementation>
    <LineIds Name="Permissives">
      <LineId Id="1454" Count="9" />
      <LineId Id="1570" Count="0" />
      <LineId Id="1569" Count="0" />
      <LineId Id="1464" Count="3" />
      <LineId Id="1568" Count="0" />
      <LineId Id="1567" Count="0" />
      <LineId Id="1468" Count="3" />
      <LineId Id="1565" Count="0" />
      <LineId Id="1564" Count="0" />
      <LineId Id="1566" Count="0" />
      <LineId Id="1631" Count="0" />
      <LineId Id="1472" Count="0" />
      <LineId Id="1632" Count="0" />
      <LineId Id="1635" Count="1" />
      <LineId Id="1638" Count="1" />
      <LineId Id="1637" Count="0" />
      <LineId Id="1643" Count="2" />
      <LineId Id="1647" Count="1" />
      <LineId Id="1650" Count="0" />
      <LineId Id="1634" Count="0" />
      <LineId Id="1473" Count="85" />
      <LineId Id="1572" Count="0" />
      <LineId Id="1399" Count="0" />
      <LineId Id="1573" Count="0" />
      <LineId Id="1711" Count="0" />
      <LineId Id="1710" Count="0" />
      <LineId Id="1779" Count="0" />
      <LineId Id="1777" Count="0" />
      <LineId Id="1781" Count="1" />
      <LineId Id="1780" Count="0" />
      <LineId Id="1784" Count="1" />
      <LineId Id="1783" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>